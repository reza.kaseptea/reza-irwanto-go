package action

import (
	"context"
	"encoding/json"
	"github.com/jinzhu/gorm"
	"movies-services/repo"
	"time"

	//"log"
	"movies-services/entity"

	"movies-services/mapping"
	pb "movies-services/transport/grpc/proto/movies"
	"movies-services/transport/http/imdb"
)

type Detail struct {
	db *gorm.DB
	logRepo repo.LogRepoContract
}

func (d *Detail) Handle(ctx context.Context, req *pb.DetailMovieRequest) (response *pb.DetailMovieResponse, err error) {
	resp := imdb.DetailMovie(req.ImdbId)
	go d.InsertLog(req, resp)
	var imdbResp entity.MovieData
	_ = json.Unmarshal([]byte(resp), &imdbResp)
	if imdbResp.Response == "" {
		return mapping.BuildResponseDetailError(imdbResp, req), nil
	}

	return mapping.BuildResponseDetail(imdbResp, req), nil
}

func (d *Detail) InsertLog(req *pb.DetailMovieRequest, resp string)  {
	var logData entity.Logging
	logData.Response = resp
	logData.Action = "Detail Movies"

	b, _ := json.Marshal(req)
	logData.Request = string(b)

	now := time.Now()
	logData.CreatedAt = &now

	d.logRepo.InsertLog(d.db, logData)
}

func NewDetail(
	db *gorm.DB,
	logRepo repo.LogRepoContract,
) *Detail {
	return &Detail{
		db: db,
		logRepo: logRepo,
	}
}
