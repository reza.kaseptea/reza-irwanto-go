package imdb

import (
	"io/ioutil"
	"movies-services/util"
	"net/http"
	"os"
)

func SearchMovie(search string, page int32) string {
	imdbUrl := os.Getenv("imdb_url")
	imdbApiKey := os.Getenv("imdb_apikey")
	url := imdbUrl+"?apikey="+imdbApiKey
	if len(search) > 0 {
		url = url + "&s="+search
	}

	if page == 0 {
		url = url + "&page=1"
	} else {
		url = url + "&page="+util.Int32ToString(page)
	}

	resp, err := http.Get(url)
	if err != nil {
		// handle error
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	sb := string(body)

	return sb

}