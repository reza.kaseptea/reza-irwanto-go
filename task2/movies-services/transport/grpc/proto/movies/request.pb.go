// Code generated by protoc-gen-go. DO NOT EDIT.
// source: request.proto

package movies

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type SearchMovieRequest struct {
	Search               string   `protobuf:"bytes,1,opt,name=search,proto3" json:"search,omitempty"`
	Pagination           int32    `protobuf:"varint,2,opt,name=pagination,proto3" json:"pagination,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *SearchMovieRequest) Reset()         { *m = SearchMovieRequest{} }
func (m *SearchMovieRequest) String() string { return proto.CompactTextString(m) }
func (*SearchMovieRequest) ProtoMessage()    {}
func (*SearchMovieRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_7f73548e33e655fe, []int{0}
}

func (m *SearchMovieRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SearchMovieRequest.Unmarshal(m, b)
}
func (m *SearchMovieRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SearchMovieRequest.Marshal(b, m, deterministic)
}
func (m *SearchMovieRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SearchMovieRequest.Merge(m, src)
}
func (m *SearchMovieRequest) XXX_Size() int {
	return xxx_messageInfo_SearchMovieRequest.Size(m)
}
func (m *SearchMovieRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_SearchMovieRequest.DiscardUnknown(m)
}

var xxx_messageInfo_SearchMovieRequest proto.InternalMessageInfo

func (m *SearchMovieRequest) GetSearch() string {
	if m != nil {
		return m.Search
	}
	return ""
}

func (m *SearchMovieRequest) GetPagination() int32 {
	if m != nil {
		return m.Pagination
	}
	return 0
}

type DetailMovieRequest struct {
	ImdbId               string   `protobuf:"bytes,1,opt,name=imdb_id,json=imdbId,proto3" json:"imdb_id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DetailMovieRequest) Reset()         { *m = DetailMovieRequest{} }
func (m *DetailMovieRequest) String() string { return proto.CompactTextString(m) }
func (*DetailMovieRequest) ProtoMessage()    {}
func (*DetailMovieRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_7f73548e33e655fe, []int{1}
}

func (m *DetailMovieRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DetailMovieRequest.Unmarshal(m, b)
}
func (m *DetailMovieRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DetailMovieRequest.Marshal(b, m, deterministic)
}
func (m *DetailMovieRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DetailMovieRequest.Merge(m, src)
}
func (m *DetailMovieRequest) XXX_Size() int {
	return xxx_messageInfo_DetailMovieRequest.Size(m)
}
func (m *DetailMovieRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_DetailMovieRequest.DiscardUnknown(m)
}

var xxx_messageInfo_DetailMovieRequest proto.InternalMessageInfo

func (m *DetailMovieRequest) GetImdbId() string {
	if m != nil {
		return m.ImdbId
	}
	return ""
}

func init() {
	proto.RegisterType((*SearchMovieRequest)(nil), "movies.SearchMovieRequest")
	proto.RegisterType((*DetailMovieRequest)(nil), "movies.DetailMovieRequest")
}

func init() { proto.RegisterFile("request.proto", fileDescriptor_7f73548e33e655fe) }

var fileDescriptor_7f73548e33e655fe = []byte{
	// 135 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0x2d, 0x4a, 0x2d, 0x2c,
	0x4d, 0x2d, 0x2e, 0xd1, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0xcb, 0xcd, 0x2f, 0xcb, 0x4c,
	0x2d, 0x56, 0xf2, 0xe1, 0x12, 0x0a, 0x4e, 0x4d, 0x2c, 0x4a, 0xce, 0xf0, 0x05, 0xf1, 0x83, 0x20,
	0x6a, 0x84, 0xc4, 0xb8, 0xd8, 0x8a, 0xc1, 0xa2, 0x12, 0x8c, 0x0a, 0x8c, 0x1a, 0x9c, 0x41, 0x50,
	0x9e, 0x90, 0x1c, 0x17, 0x57, 0x41, 0x62, 0x7a, 0x66, 0x5e, 0x62, 0x49, 0x66, 0x7e, 0x9e, 0x04,
	0x93, 0x02, 0xa3, 0x06, 0x6b, 0x10, 0x92, 0x88, 0x92, 0x2e, 0x97, 0x90, 0x4b, 0x6a, 0x49, 0x62,
	0x66, 0x0e, 0x8a, 0x69, 0xe2, 0x5c, 0xec, 0x99, 0xb9, 0x29, 0x49, 0xf1, 0x99, 0x29, 0x30, 0xe3,
	0x40, 0x5c, 0xcf, 0x94, 0x24, 0x36, 0xb0, 0x5b, 0x8c, 0x01, 0x01, 0x00, 0x00, 0xff, 0xff, 0x14,
	0xdc, 0x06, 0xfb, 0x9c, 0x00, 0x00, 0x00,
}
