package entity

type Pagination struct {
	Page  	 	int32      `json:"page"`
	TotalPage   int32      `json:"total_page"`
	TotalData   int32      `json:"total_data"`
	PerPage     int32 	   `json:"per_page"`
}