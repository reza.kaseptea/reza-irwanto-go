package client

import (
	"context"
	"go.elastic.co/apm/module/apmgrpc"
	"os"

	pb "gateway-api/proto/movies"
	"google.golang.org/grpc"
)

func SearchMovie(ctx context.Context, req *pb.SearchMovieRequest) (*pb.SearchMovieResponse, error) {
	conn, err := grpc.Dial(os.Getenv("movies_service"), grpc.WithInsecure(), grpc.WithUnaryInterceptor(apmgrpc.NewUnaryClientInterceptor()))
	var resp pb.SearchMovieResponse
	if err != nil {
		return &resp, err
	}

	defer conn.Close()

	client := pb.NewMovieServiceClient(conn)
	return client.SearchMovie(ctx, req)
}

func DetailMovie(ctx context.Context, req *pb.DetailMovieRequest) (*pb.DetailMovieResponse, error) {
	conn, err := grpc.Dial(os.Getenv("movies_service"), grpc.WithInsecure(), grpc.WithUnaryInterceptor(apmgrpc.NewUnaryClientInterceptor()))
	var resp pb.DetailMovieResponse
	if err != nil {
		return &resp, err
	}

	defer conn.Close()

	client := pb.NewMovieServiceClient(conn)
	return client.DetailMovie(ctx, req)
}
