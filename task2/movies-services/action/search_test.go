package action

import (
	"context"
	"database/sql"
	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
	"github.com/stretchr/testify/mock"
	"movies-services/entity"
	pb "movies-services/transport/grpc/proto/movies"
	"time"

	"github.com/stretchr/testify/require"

	"testing"

	"github.com/DATA-DOG/go-sqlmock"

	"github.com/stretchr/testify/suite"
	"movies-services/repo/mocks"
)

type SearchTest struct {
	suite.Suite
	DB    *gorm.DB
	iRepo *mocks.LogRepoContract
	act   *Search
}

func (s *SearchTest) SetupTest() {
	var (
		db  *sql.DB
		err error
	)

	db, _, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open("mysql", db)
	require.NoError(s.T(), err)

	s.iRepo = new(mocks.LogRepoContract)
	s.act = NewSearch(s.DB, s.iRepo)
	godotenv.Load("../test.env")
}

func TestSearchTest(t *testing.T) {
	suite.Run(t, new(SearchTest))
}

func (s *SearchTest) AfterTest(_, _ string) {

}

func (s *SearchTest) TestResultSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var dataRequest pb.SearchMovieRequest
	dataRequest.Search = "Batman"
	dataRequest.Pagination = 1

	var dataLog entity.Logging

	s.iRepo.On("InsertLog", s.DB, mock.IsType(dataLog))
	_, err := s.act.Handle(ctx, &dataRequest)

	s.iRepo.AssertNumberOfCalls(s.T(), "InsertLog", 1)
	require.NoError(s.T(), err)

}
