package handler

import (
	"context"
	"gateway-api/domain/movies/mapping"
	"log"
	"net/http"

	"gateway-api/domain/movies/client"
	pb "gateway-api/proto/movies"
	"gateway-api/util"

	"github.com/labstack/echo/v4"
)

// Auth handler
type SearchMovies struct{}

// Handle is a handler for generating qr code
func (h *SearchMovies) Handle(c echo.Context) (err error) {
	var resp mapping.MoviesMapping
	var req pb.SearchMovieRequest
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	req.Search = c.QueryParam("search")
	req.Pagination = util.StringToInt32(c.QueryParam("page"))
	grpcResp, err := client.SearchMovie(ctx, &req)

	if grpcResp.ErrorResponse != nil {
		log.Println("err", grpcResp.ErrorResponse.Errors)
		data := map[string]interface{}{
			"error_response": grpcResp.ErrorResponse.Errors,
		}
		resp.Code = int(grpcResp.ErrorResponse.Status)
		resp.Data = data
		return c.JSON(int(grpcResp.ErrorResponse.HttpStatus), resp)
	}


	data := map[string]interface{}{
		"search": grpcResp.Search,
		"pagination": grpcResp.Pagination,
	}
	resp.Data = data
	resp.Code = http.StatusOK
	resp.Message = "Berhasil Memuat Data"
	resp.Errors = nil

	return c.JSON(http.StatusOK, resp)
}

// NewWorld new
func NewSearchMovies() *SearchMovies {
	return &SearchMovies{}
}
