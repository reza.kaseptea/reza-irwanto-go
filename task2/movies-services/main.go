package main

import (
	grpcMiddleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/joho/godotenv"
	"go.elastic.co/apm/module/apmgrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	grpcServer "movies-services/transport/grpc"
	pb "movies-services/transport/grpc/proto/movies"
	"movies-services/util"
	"net"
	"os"
	"time"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	lis, errs := net.Listen("tcp", ":"+os.Getenv("app_port_grpc"))
	if errs != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	log.Println("Connect GRPC to port " + os.Getenv("app_port_grpc"))

	host := os.Getenv("db_host")
	port := os.Getenv("db_port")
	user := os.Getenv("db_user")
	password := os.Getenv("db_password")
	dbname := os.Getenv("db_database")

	conn, err := gorm.Open("mysql", user+":"+password+"@tcp("+host+":"+port+")/"+dbname)
	defer conn.Close()
	log.Println(conn)
	log.Println(err)
	if err != nil {
		panic(err)
	}

	db := conn

	db.DB().SetConnMaxLifetime(time.Minute * 5)
	db.DB().SetMaxIdleConns(5)
	db.DB().SetMaxOpenConns(10)

	log.Println("Successfully connected!")


	log.Println("GRPC Server is Connected !!!")

	if os.Getenv("debug_gorm") == "true" {
		db.LogMode(true)
		db.LogMode(true)
	}

	globalEnv := &util.Env{
		Db:      db,
	}

	s := grpc.NewServer(grpc.UnaryInterceptor(grpcMiddleware.ChainUnaryServer(
		apmgrpc.NewUnaryServerInterceptor(apmgrpc.WithRecovery()))))
	pb.RegisterMovieServiceServer(s, grpcServer.NewServer(globalEnv))

	// Register reflection service on gRPC server.
	reflection.Register(s)
	if errServe := s.Serve(lis); errServe != nil {
		log.Fatalf("failed to serve: %v", errServe)
	} else {
		log.Println("GRPC Server is Connected !!!")
	}
}
