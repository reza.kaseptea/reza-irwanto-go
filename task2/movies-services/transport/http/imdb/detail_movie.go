package imdb

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func DetailMovie(imdbID string) string {
	imdbUrl := os.Getenv("imdb_url")
	imdbApiKey := os.Getenv("imdb_apikey")
	url := imdbUrl+"?apikey="+imdbApiKey
	if len(imdbID) > 0 {
		url = url + "&i="+imdbID
	}
	log.Println(url)
	resp, err := http.Get(url)
	if err != nil {
		// handle error
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	sb := string(body)

	return sb

}