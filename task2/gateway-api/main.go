package main

import (
	"github.com/joho/godotenv"
	"log"

	"gateway-api/route"
	"os"
)

func main() {

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	e := route.Init()
	e.Logger.Fatal(e.Start(":" + os.Getenv("app_port")))
	//r.Start(":"+os.Getenv("app_port"))
}
