module gateway-api

go 1.12

require (
	github.com/golang/protobuf v1.3.3
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo/v4 v4.1.13
	github.com/satori/go.uuid v1.2.0 // indirect
	go.elastic.co/apm/module/apmechov4 v1.7.0
	go.elastic.co/apm/module/apmgrpc v1.7.0
	golang.org/x/lint v0.0.0-20190313153728-d0100b6bd8b3 // indirect
	golang.org/x/tools v0.0.0-20190524140312-2c0ae7006135 // indirect
	google.golang.org/grpc v1.27.0
	honnef.co/go/tools v0.0.0-20190523083050-ea95bdfd59fc // indirect
)
