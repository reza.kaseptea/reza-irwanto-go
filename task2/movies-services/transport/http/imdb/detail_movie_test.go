package imdb

import (
	"github.com/joho/godotenv"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"testing"

	"github.com/stretchr/testify/suite"
)

type DetailTestSuite struct {
	suite.Suite
}

func (s *DetailTestSuite) SetupTest() {
	godotenv.Load("../../../test.env")
}

func TestDetailTestSuite(t *testing.T) {
	suite.Run(t, new(DetailTestSuite))
}

func (s *DetailTestSuite) AfterTest(_, _ string) {

}

func (s *DetailTestSuite) TestResultSuccess() {
	call := mock.Call{Method: DetailMovie("tt6751668")}
	require.NotNil(s.T(), call)

}
