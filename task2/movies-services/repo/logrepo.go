package repo

import (
	"github.com/jinzhu/gorm"
	"movies-services/entity"

)

type LogRepo struct {
	db      *gorm.DB
}

// FindByEmail in user
func (st *LogRepo) InsertLog(db *gorm.DB, logData entity.Logging ){
	_ = st.db.Create(logData).Error
}

func NewLogRepo(db *gorm.DB) *LogRepo {
	LogRepo := &LogRepo{
		db:      db,
	}

	return LogRepo
}
