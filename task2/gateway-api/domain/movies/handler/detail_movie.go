package handler

import (
	"context"
	"gateway-api/domain/movies/mapping"
	"log"
	"net/http"

	"gateway-api/domain/movies/client"
	pb "gateway-api/proto/movies"
	"github.com/labstack/echo/v4"
)

// Auth handler
type DetailMovies struct{}

// Handle is a handler for generating qr code
func (h *DetailMovies) Handle(c echo.Context) (err error) {
	var resp mapping.MoviesMapping
	var req pb.DetailMovieRequest
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	req.ImdbId = c.Param("imdb_id")
	grpcResp, err := client.DetailMovie(ctx, &req)

	if grpcResp.ErrorResponse != nil {
		log.Println("err", grpcResp.ErrorResponse.Errors)
		data := map[string]interface{}{
			"error_response": grpcResp.ErrorResponse.Errors,
		}
		resp.Code = int(grpcResp.ErrorResponse.Status)
		resp.Data = data
		return c.JSON(int(grpcResp.ErrorResponse.HttpStatus), resp)
	}


	data := map[string]interface{}{
		"movie_detail": grpcResp.Movie,
	}
	resp.Data = data
	resp.Code = http.StatusOK
	resp.Message = "Berhasil Memuat Data"
	resp.Errors = nil

	return c.JSON(http.StatusOK, resp)
}

// NewWorld new
func NewDetailMovies() *DetailMovies {
	return &DetailMovies{}
}
