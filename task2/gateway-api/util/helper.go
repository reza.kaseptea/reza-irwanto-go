package util

import (
	"log"
	"strconv"
)

func Int32ToString (number int32) string {
	i := strconv.Itoa(int(number))
	log.Println(i)
	return i
}

func StringToFloat64(txt string) float64 {
	i, _ := strconv.ParseFloat(txt, 64)
	return i
}

func StringToInt32 (number string) int32 {
	i, _ := strconv.Atoi(number)
	log.Println(i)
	return int32(i)
}