package mapping

type MoviesMapping struct {
	Code    int                   		`json:"code"`
	Message string                		`json:"message,omitempty"`
	Data    map[string]interface{} 		`json:"data,omitempty"`
	Errors  []string              		`json:"errors,omitempty"`
}