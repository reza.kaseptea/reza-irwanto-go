package imdb

import (
	"github.com/joho/godotenv"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"testing"

	"github.com/stretchr/testify/suite"
)

type SearchTestSuite struct {
	suite.Suite
}

func (s *SearchTestSuite) SetupTest() {
	godotenv.Load("../../../test.env")
}

func TestSearchTestSuite(t *testing.T) {
	suite.Run(t, new(SearchTestSuite))
}

func (s *SearchTestSuite) AfterTest(_, _ string) {

}

func (s *SearchTestSuite) TestResultSuccess() {
	call := mock.Call{Method: SearchMovie("Batman", 1)}
	require.NotNil(s.T(), call)

	call2 := mock.Call{Method: SearchMovie("Batman", 0)}
	require.NotNil(s.T(), call2)

}
