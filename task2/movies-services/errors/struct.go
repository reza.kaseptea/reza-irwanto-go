package errors

import (
	"google.golang.org/grpc/status"
	"net/http"
)

// Error struct
type Error struct{}

// ErrNotFound is for requested data is not found
func (e *Error) ErrNotFound(message string) error {
	return status.Errorf(http.StatusNotFound, message+" tidak ditemukan")
}

// ErrInternalServer is internal server error
func (e *Error) ErrInternalServer(message string) error {
	return status.Errorf(http.StatusInternalServerError, "Mohon Maaf. "+message)
}

// ErrForbidden func
func (e *Error) ErrForbidden(message string) error {
	return status.Errorf(http.StatusForbidden, message)
}

// ErrBadRequest func
func (e *Error) ErrBadRequest(message string) error {
	return status.Errorf(http.StatusBadRequest, message)
}

// ErrValidationError is for validation failed occurred
// func (e *Error) ErrValidationErrors(errors map[string][]string) error {
//	return status.Errorf(codes.InvalidArgument, "Gagal validasi data"+errors)
//	return NewErrors("Gagal validasi data", errors, http.StatusBadRequest)
// }

// ErrValidationError is for validation failed occurred
func (e *Error) ErrValidationError(error string) error {
	return status.Errorf(http.StatusBadRequest, "Gagal validasi data: "+error)
}

// ErrTokenExpiredError is for validation failed occurred
func (e *Error) ErrTokenExpiredError(error string) error {
	return status.Errorf(http.StatusUnauthorized, error)
}

// ErrMenuNotAuthorized is not authorized
func (e *Error) ErrMenuNotAuthorized(message string) error {
	return status.Errorf(http.StatusUnauthorized, message)
}

// ErrRoleNameExists is not authorized
func (e *Error) ErrRoleNameExists(message string) error {
	return status.Errorf(http.StatusUnauthorized, message)
}

// ErrRoleUpdate is not authorized
func (e *Error) ErrRoleUpdate(message string) error {
	return status.Errorf(http.StatusBadRequest, message)
}

// ErrUserTypeNotHaveAdmin is not authorized
func (e *Error) ErrUserTypeNotHaveAdmin(message string) error {
	return status.Errorf(http.StatusBadRequest, message)
}

// ErrNotFound is for requested data is not found
func (e *Error) ErrNotFoundCustomMessages(message string) error {
	return status.Errorf(http.StatusNotFound, message)
}

// ErrMenuNotAuthorized is not authorized
func (e *Error) ErrInactiveAccountOrData(message string) error {
	return status.Errorf(http.StatusGone, message)
}

func (e *Error) ErrInternalServerError(message string) error {
	return status.Errorf(http.StatusInternalServerError, "Terjadi kesalahan server: "+message)
}
