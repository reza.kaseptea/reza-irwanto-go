package mapping

import (
	"encoding/json"
	"math"

	"movies-services/entity"
	error2 "movies-services/errors"
	pb "movies-services/transport/grpc/proto/movies"
	"movies-services/util"
)

func BuildResponseSearch(d entity.ImdbSearch, req *pb.SearchMovieRequest) *pb.SearchMovieResponse {
	var pagin entity.Pagination
	var resp pb.SearchMovieResponse
	perPage := len(d.Search)
	totalData := util.StringToFloat64(d.TotalResuls)

	totalPage := int32(math.Ceil(totalData / float64(perPage)))
	pagin.TotalData = int32(totalData)
	pagin.TotalPage = totalPage
	pagin.Page = req.Pagination
	pagin.PerPage = int32(perPage)

	bytes, _ := json.Marshal(&pagin)
	json.Unmarshal(bytes, &resp.Pagination)

	bytes, _ = json.Marshal(&d.Search)
	json.Unmarshal(bytes, &resp.Search)

	resp.Response = d.Response
	resp.TotalResults = util.StringToInt32(d.TotalResuls)

	resp.Status = 200

	return &resp
}

func BuildResponseErrorSearch(d entity.ImdbSearch, req *pb.SearchMovieRequest) *pb.SearchMovieResponse {
	var resp pb.SearchMovieResponse
	var e *error2.Error
	var err pb.ErrorResponse

	err.Status = 500
	err.HttpStatus = 500
	err.Errors = e.ErrBadRequest("Internal server error, please check your request").Error()
	resp.ErrorResponse = &err
	resp.Status = 500

	return &resp
}