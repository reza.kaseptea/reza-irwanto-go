package main

import (
	"log"
	"runtime"
	"strings"
)

type Anagram struct {
	Word []string
}

func main()  {
	// set core goroutine
	runtime.GOMAXPROCS(2)

	// object resp array
	var resp []Anagram

	// object to handle last array
	var last Anagram

	// string array input
	input := []string{"kita", "atik", "tika", "aku", "kia", "makan", "kua"}

	// loop until input == 1
	for len(input) > 1 {
		resp, input = prosess(resp, input)
	}

	// set last object input to last array
	last.Word = append(last.Word, input[0])
	resp = append(resp, last)

	// print out response
	log.Println(resp)
}

func prosess(respData []Anagram, input []string) ([]Anagram, []string){
	var resp Anagram
	inputSlice := input
	// handling if input len is the last element
	if len(input) == 1 {
		//append to obj single resp
		resp.Word = append(resp.Word, input[0])
		//pop array
		inputSlice = removeArr(inputSlice, input[0])

		//set object resp
		respData = append(respData, resp)
	} else {
		for _, val := range input {
			for _, val2 := range input {
				// init channel object
				var status = make(chan bool)

				// split string to array
				s1 := strings.Split(val, "")
				s2 := strings.Split(val2, "")

				// run go routine to check the words
				go CheckAnagram(s1, val2, status)
				go CheckAnagram(s2, val, status)

				// set return resp go routine
				var status1 = <-status
				var status2 = <-status
				if status2 == true && status1 == true {
					resp.Word = append(resp.Word, val2)
					inputSlice = removeArr(inputSlice, val2)
				}

			}

			// handling if struct object not nill
			if resp.Word != nil {
				// append to resp object
				respData = append(respData, resp)
			} else {
				// handling if no words are match
				// set to temp object
				resp.Word = append(resp.Word, val)

				// append to resp object
				respData = append(respData, resp)

				// pop object array
				inputSlice = removeArr(inputSlice, val)
			}
			// break loop
			break
		}
	}

	// return data
	return respData, inputSlice
}

// remove array
func removeArr(arr []string, val string) []string {
	ret := make([]string, 0)
	rk := getKey(arr, val)
	if rk >= 0 {
		ret = append(ret, arr[:rk]...)
		arr = append(ret, arr[rk+1:]...)
	}

	return arr
}

// this function to check the first string and second string are match
func CheckAnagram(str1 []string, str2 string, objChan chan bool)  {
	if len(str1) == len(str2) {
		status := true
		for _, val := range str1 {
			if strings.Contains(str2, val) == false {
				status = false
				break
			}
		}
		objChan <- status
	} else {
		objChan <- false
	}
}

// this function to get key off array
func getKey(arr []string, str string) int {
	for key, a := range arr {
		if a == str {
			return key
		}
	}
	return -1
}