package mapping

import (
	"encoding/json"
	"movies-services/entity"
	error2 "movies-services/errors"
	pb "movies-services/transport/grpc/proto/movies"
)

func BuildResponseDetail(d entity.MovieData, req *pb.DetailMovieRequest) *pb.DetailMovieResponse {

	var resp pb.DetailMovieResponse
	resp.Status = 200

	bytes, _ := json.Marshal(&d)
	json.Unmarshal(bytes, &resp.Movie)

	return &resp
}

func BuildResponseDetailError(d entity.MovieData, req *pb.DetailMovieRequest) *pb.DetailMovieResponse {
	var resp pb.DetailMovieResponse
	var e *error2.Error
	var err pb.ErrorResponse

	err.Status = 500
	err.HttpStatus = 500
	err.Errors = e.ErrBadRequest("Internal server error, please check your request").Error()
	resp.ErrorResponse = &err
	resp.Status = 500

	return &resp
}