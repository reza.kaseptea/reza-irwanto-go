package main

import (
	"log"
	"strings"
)

func main() {
	returnData := findFirstStringInBracket("(abcd)")
	log.Println(returnData)
}

func findFirstStringInBracket(str string) string {
	if len(str) > 0 {
		runes := []rune(str)
		indexFirstBracketFound := strings.Index(str,"(")
		indexClosingBracketFound := strings.Index(str,")")
		if indexFirstBracketFound < 0 || indexClosingBracketFound < 0 {
			return "there is no open bracket or close bracket or both"
		}
		return string(runes[indexFirstBracketFound+1:indexClosingBracketFound])
	}else{
		return ""
	}

}