package repo

import (

	"github.com/jinzhu/gorm"
	"movies-services/entity"
)

type LogRepoContract interface {
	InsertLog(*gorm.DB, entity.Logging)
}