package entity

type ImdbSearch struct {
	Search  	[]MovieSearch   `json:"Search"`
	TotalResuls string        	`json:"totalResults"`
	Response    string     		`json:"Response"`
}

type MovieSearch struct {
	Title  	 string     `json:"Title"`
	Year     string     `json:"Year"`
	ImdbID   string     `json:"imdbID"`
	Type     string 	`json:"Type"`
	Poster   string 	`json:"Poster"`
}

type MovieData struct {
	Title 		string 			`json:"Title"`
	Year  		string 			`json:"Year"`
	Rated 		string 			`json:"Rated"`
	Releases 	string 			`json:"Releases"`
	Runtime 	string  		`json:"Runtime"`
	Genre 		string  		`json:"Genre"`
	Director	string 			`json:"Director"`
	Writer 		string 			`json:"Writer"`
	Actors 		string 			`json:"Actors"`
	Plot		string 			`json:"Plot"`
	Language	string 			`json:"Language"`
	Country 	string 			`json:"Country"`
	Awards		string 			`json:"Awards"`
	Poster		string 			`json:"Poster"`
	Ratings 	[]MovieRatings 	`json:"Ratings"`
	Metascore 	string 			`json:"Metascore"`
	ImdbRating	string 			`json:"ImdbRating"`
	ImdbVotes	string 			`json:"ImdbVotes"`
	ImdbID		string 			`json:"ImdbID"`
	Type		string 			`json:"Type"`
	Dvd 		string 			`json:"DVD"`
	BoxOffice	string 			`json:"BoxOffice"`
	Production	string 			`json:"Production"`
	Website		string 			`json:"Website"`
	Response	string 			`json:"Response"`
}

type MovieRatings struct {
	Source string `json:"Source"`
	Value  string `json:"Value"`
}
