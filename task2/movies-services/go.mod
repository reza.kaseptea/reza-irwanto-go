module movies-services

go 1.12

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/golang/protobuf v1.3.3
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.0
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.3.0 // indirect
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.5.1
	go.elastic.co/apm v1.9.0 // indirect
	go.elastic.co/apm/module/apmgrpc v1.7.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9 // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/grpc v1.27.0
)
