package entity

import (
	"time"
)

type Logging struct {
	Action   	string      `gorm:"column:action;" json:"action"`
	Request  	string     	`gorm:"column:request;" json:"request"`
	Response 	string     	`gorm:"column:response;" json:"response"`
	CreatedAt   *time.Time  `gorm:"column:created_at;" json:"-"`
}

func (Logging) TableName() string {
	return "logs"
}