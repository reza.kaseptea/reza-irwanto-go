package grpc

import (
	"context"
	"log"

	//"go.elastic.co/apm/module/apmgorm"
	"movies-services/action"
	"movies-services/repo"
	pb "movies-services/transport/grpc/proto/movies"
	"movies-services/util"
)

// Server: create
type Server struct {
	env *util.Env
}

func (gs *Server) SearchMovie(ctx context.Context, req *pb.SearchMovieRequest) (*pb.SearchMovieResponse, error) {
	logRepo := repo.NewLogRepo(gs.env.Db)
	d, err := action.NewSearch(gs.env.Db, logRepo).Handle(ctx, req)
	if err != nil {
		return nil, err
	}

	log.Println(d)

	return d,nil
}

func (gs *Server) DetailMovie(ctx context.Context, req *pb.DetailMovieRequest) (*pb.DetailMovieResponse, error) {
	logRepo := repo.NewLogRepo(gs.env.Db)
	d, err := action.NewDetail(gs.env.Db, logRepo).Handle(ctx, req)
	if err != nil {
		return nil, err
	}

	log.Println(d)

	return d,nil
}

// NewServer create new Server
func NewServer(env *util.Env) *Server {
	return &Server{env: env}
}

