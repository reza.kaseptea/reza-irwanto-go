package action

import (
	"context"
	"encoding/json"
	"github.com/jinzhu/gorm"
	"movies-services/repo"
	"time"

	//"log"
	"movies-services/entity"

	"movies-services/mapping"
	pb "movies-services/transport/grpc/proto/movies"
	"movies-services/transport/http/imdb"
)

type Search struct {
	db *gorm.DB
	logRepo repo.LogRepoContract
}

func (d *Search) Handle(ctx context.Context, req *pb.SearchMovieRequest) (response *pb.SearchMovieResponse, err error) {

	resp := imdb.SearchMovie(req.Search, req.Pagination)
	go d.InsertLog(req, resp)
	var imdbResp entity.ImdbSearch
	_ = json.Unmarshal([]byte(resp), &imdbResp)
	if imdbResp.Response == "" {
		return mapping.BuildResponseErrorSearch(imdbResp, req), nil
	}

	return mapping.BuildResponseSearch(imdbResp, req), nil
}

func (d *Search) InsertLog(req *pb.SearchMovieRequest, resp string)  {
	var logData entity.Logging
	logData.Response = resp
	logData.Action = "Search Movies"

	b, _ := json.Marshal(req)
	logData.Request = string(b)

	now := time.Now()
	logData.CreatedAt = &now

	d.logRepo.InsertLog(d.db, logData)
}

func NewSearch(
	db *gorm.DB,
	logRepo repo.LogRepoContract,
	) *Search {
	return &Search{
		db: db,
		logRepo: logRepo,
	}
}
