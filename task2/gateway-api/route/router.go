package route

import (
	logic "gateway-api/domain/movies/handler"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"go.elastic.co/apm/module/apmechov4"
)

// Contract endpoint to use it later
type Contract interface {
	Handle(c echo.Context) (err error)
}

var endpoint = map[string]Contract{
	// auth
	"search_movie":        logic.NewSearchMovies(),
	"detail_movie":        logic.NewDetailMovies(),
}

// Init gateway router
func Init() *echo.Echo {
	e := echo.New()

	// Set Bundle MiddleWare
	e.Use(middleware.RequestID())
	e.Pre(middleware.RemoveTrailingSlash())
	e.Use(middleware.Gzip())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, echo.HeaderAuthorization},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))

	e.Use(middleware.Recover())
	e.Use(apmechov4.Middleware())

	prefix := "/api/v1"
	task1Prefix := "/movies"

	// auth endpoint
	e.GET(prefix+task1Prefix+"/search", endpoint["search_movie"].Handle)
	e.GET(prefix+task1Prefix+"/:imdb_id", endpoint["detail_movie"].Handle)

	return e
}
