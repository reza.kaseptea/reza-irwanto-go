// Code generated by protoc-gen-go. DO NOT EDIT.
// source: movies.proto

package movies

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

func init() { proto.RegisterFile("movies.proto", fileDescriptor_546d72ade507cae9) }

var fileDescriptor_546d72ade507cae9 = []byte{
	// 132 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0xc9, 0xcd, 0x2f, 0xcb,
	0x4c, 0x2d, 0xd6, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x83, 0xf0, 0xa4, 0x78, 0x8b, 0x52,
	0x0b, 0x4b, 0x53, 0x8b, 0x4b, 0x20, 0xc2, 0x52, 0x7c, 0x45, 0xa9, 0xc5, 0x05, 0xf9, 0x79, 0xc5,
	0xa9, 0x10, 0xbe, 0xd1, 0x3c, 0x46, 0x2e, 0x1e, 0x5f, 0x90, 0xca, 0xe0, 0xd4, 0xa2, 0xb2, 0xcc,
	0xe4, 0x54, 0x21, 0x37, 0x2e, 0xee, 0xe0, 0xd4, 0xc4, 0xa2, 0xe4, 0x0c, 0xb0, 0xa8, 0x90, 0x94,
	0x1e, 0xd4, 0x54, 0x24, 0xc1, 0x20, 0x88, 0x89, 0x52, 0xd2, 0x58, 0xe5, 0x20, 0xc6, 0x83, 0xcc,
	0x71, 0x49, 0x2d, 0x49, 0xcc, 0xcc, 0x41, 0x33, 0x07, 0x49, 0x10, 0xc3, 0x1c, 0x14, 0x39, 0x88,
	0x39, 0x49, 0x6c, 0x60, 0x77, 0x1a, 0x03, 0x02, 0x00, 0x00, 0xff, 0xff, 0xfc, 0x9d, 0x2e, 0x5b,
	0xde, 0x00, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// MovieServiceClient is the client API for MovieService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type MovieServiceClient interface {
	SearchMovie(ctx context.Context, in *SearchMovieRequest, opts ...grpc.CallOption) (*SearchMovieResponse, error)
	DetailMovie(ctx context.Context, in *DetailMovieRequest, opts ...grpc.CallOption) (*DetailMovieResponse, error)
}

type movieServiceClient struct {
	cc *grpc.ClientConn
}

func NewMovieServiceClient(cc *grpc.ClientConn) MovieServiceClient {
	return &movieServiceClient{cc}
}

func (c *movieServiceClient) SearchMovie(ctx context.Context, in *SearchMovieRequest, opts ...grpc.CallOption) (*SearchMovieResponse, error) {
	out := new(SearchMovieResponse)
	err := c.cc.Invoke(ctx, "/movies.MovieService/SearchMovie", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *movieServiceClient) DetailMovie(ctx context.Context, in *DetailMovieRequest, opts ...grpc.CallOption) (*DetailMovieResponse, error) {
	out := new(DetailMovieResponse)
	err := c.cc.Invoke(ctx, "/movies.MovieService/DetailMovie", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// MovieServiceServer is the server API for MovieService service.
type MovieServiceServer interface {
	SearchMovie(context.Context, *SearchMovieRequest) (*SearchMovieResponse, error)
	DetailMovie(context.Context, *DetailMovieRequest) (*DetailMovieResponse, error)
}

// UnimplementedMovieServiceServer can be embedded to have forward compatible implementations.
type UnimplementedMovieServiceServer struct {
}

func (*UnimplementedMovieServiceServer) SearchMovie(ctx context.Context, req *SearchMovieRequest) (*SearchMovieResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SearchMovie not implemented")
}
func (*UnimplementedMovieServiceServer) DetailMovie(ctx context.Context, req *DetailMovieRequest) (*DetailMovieResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DetailMovie not implemented")
}

func RegisterMovieServiceServer(s *grpc.Server, srv MovieServiceServer) {
	s.RegisterService(&_MovieService_serviceDesc, srv)
}

func _MovieService_SearchMovie_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SearchMovieRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MovieServiceServer).SearchMovie(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/movies.MovieService/SearchMovie",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MovieServiceServer).SearchMovie(ctx, req.(*SearchMovieRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MovieService_DetailMovie_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DetailMovieRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MovieServiceServer).DetailMovie(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/movies.MovieService/DetailMovie",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MovieServiceServer).DetailMovie(ctx, req.(*DetailMovieRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _MovieService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "movies.MovieService",
	HandlerType: (*MovieServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SearchMovie",
			Handler:    _MovieService_SearchMovie_Handler,
		},
		{
			MethodName: "DetailMovie",
			Handler:    _MovieService_DetailMovie_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "movies.proto",
}
