package repo

import (
	"database/sql"
	"github.com/joho/godotenv"
	"github.com/stretchr/testify/mock"
	"movies-services/entity"
	"movies-services/repo/mocks"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/require"

	"github.com/stretchr/testify/suite"
	"testing"
)

type j map[string]interface{}

type MerchantCriteriaTestSuite struct {
	suite.Suite
	DB *gorm.DB
	dbMock sqlmock.Sqlmock
	iRepo *mocks.LogRepoContract
	error error
	time time.Time
	act *LogRepo

}

func (s *MerchantCriteriaTestSuite) SetupTest() {
	var (
		db  *sql.DB
		err error
	)

	db, _, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open("mysql", db)
	require.NoError(s.T(), err)

	s.iRepo = new(mocks.LogRepoContract)
	s.act = NewLogRepo(s.DB)
	godotenv.Load("../test.env")
}

func TestMerchantCriteriaTestSuite(t *testing.T) {
	suite.Run(t, new(MerchantCriteriaTestSuite))
}

func (s *MerchantCriteriaTestSuite) AfterTest(_, _ string) {
	//s.iRepo.AssertExpectations(s.T())
	//require.NoError(s.T(), s.dbMock.ExpectationsWereMet())
}


func (s *MerchantCriteriaTestSuite) TestResultSuccess() {
	var dataLog entity.Logging
	s.iRepo.On("InsertLog", s.DB, mock.IsType(dataLog))

	s.act.InsertLog(s.DB, dataLog)
	s.iRepo.AssertNumberOfCalls(s.T(), "InsertLog", 0)
}
